import time
import numpy as np 

random_array = np.random.randint(low=0, high=10, size=30) 
random_number = np.random.randint(0, 10)

inicio = time.time()
step = 0
flag = 0
for i in range(len(random_array)):
    step += 1
    if random_number == i:
        fin = time.time()
        flag = 1
        print("Lineal: Se encontró el número", random_number, "en la posición", random_array[i], "en", step, "iteraciones.", fin-inicio)
if not flag:
    fin = time.time()
    print("Lineal: Elemento no encontrado.", fin-inicio)

def search(array, value):
	steps = 0 
	left = 0 
	rigth = len(array) - 1 

	while left <= rigth: 
		steps += 1 
		middle = (left + rigth) // 2 
		if array[middle] == value: 
			return "Binario: Valor {} encontrado en {} iteraciones, en la posición {}.".format(value, steps, middle) 
		if array[middle] > value: 
			rigth = middle - 1 
		if array[middle] < value: 
			left = middle + 1 
	return "Binario: Elemento no encontrado." 
random_array.sort() 

inicio1 = time.time()
result = search(random_array, random_number)
fin1 = time.time()

print (result, fin1-inicio1)